var express = require('express');
var app = express();
var server = app.listen(3000, () => {
    console.log('Listening on port ' + server.address().port);
});
var fs = require('fs');
app.use(express.static('client'));

console.log('Serveur running !');


var io = require('socket.io').listen(server);

var games = {};
/*var Plateforme = require('./class/Plateforme.class.js');
var Game = require('./class/Game.class.js');
var Player = require('./class/Player.class.js');*/

io.sockets.on('connection', function (socket) {
    //Variable uniquement utilisée par le PC Host
    var pc_token;
    //Variable uniquement utilisées par les mobiles
    var mobile_pc_token;
    var mobile_pc_socket_id;

    socket.on('create_game_submit', (name) => {
        //var new_token = Math.floor(Math.random() * (999999 - 100000 + 1) + 100000);
        var new_token = 111111;
        //s'assure que le token n'est pas déjà utilisé
        while (new_token in games) {
            new_token = Math.floor(Math.random() * (999999 - 100000 + 1) + 100000);
        }
        pc_token = new_token;
        games[pc_token] = {};
        games[pc_token].name = name;
        //Liste des joueurs mobile
        games[pc_token].players = {};
        //Socket id du pc qui host la partie
        games[pc_token].socket_id = socket.id;
        //Couleurs qui seront attribuées aux joueurs qui rejoindront la partie
        games[pc_token].colors = ['#e67e22', '#c0392b', '#95a5a6', '#f1c40f', '#3498db', '#2ecc71', '#9b59b6', '#1abc9c'];
        shuffle(games[pc_token].colors);
        socket.emit('create_game_done', { token: pc_token, name: name });
    });

    socket.on('hello_mobile_check_game_exists', (token_to_check) => {
        if (token_to_check in games) {
            //Le mobile rejoint la partie
            games[token_to_check].players[socket.id] = {};
            games[token_to_check].players[socket.id].color = games[token_to_check].colors[0];
            games[token_to_check].colors.splice(0, 1);
            mobile_pc_socket_id = games[token_to_check].socket_id;
            mobile_pc_token = token_to_check;
            socket.emit('hello_mobile_check_game_exists_answer', {
                answer: true,
                color: games[token_to_check].players[socket.id].color
            });
            io.to(games[token_to_check].socket_id).emit('waiting_room_player_joined', {
                color: games[token_to_check].players[socket.id].color,
                nb_players_connected: Object.keys(games[token_to_check].players).length
            });
        } else {
            socket.emit('hello_mobile_check_game_exists_answer', false);
        }
    });

    socket.on('hot_potato_start_require_info', () => {
        socket.emit('hot_potato_start_give_info', {
            nb_players: Object.keys(games[pc_token].players).length,
            players: games[pc_token].players
        });
    });

    socket.on('controls_move', (e) => {
        io.to(mobile_pc_socket_id).emit('player_move', {
            socket_id: socket.id,
            move: e.move
        });
    });

    socket.on('controls_unload', () => {
        console.log(socket.id + ' has disconnected');
        console.log('token : ' + mobile_pc_token);
        if(mobile_pc_token in games) {
            delete games[mobile_pc_token].players[socket.id];
            io.to(mobile_pc_socket_id).emit('player_unload', {
                socket_id: socket.id,
                nb_players_connected: Object.keys(games[mobile_pc_token].players).length
            });
        }
    });

    socket.on('pc_unload', () => {
        if(pc_token in games) {
            console.log('PC has unloaded');
            for(var mobile in games[pc_token].players) {
                io.to(mobile).emit('pc_unload');
            }
        }
    });
});

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}
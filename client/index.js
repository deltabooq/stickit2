//const game_canvas = document.getElementById('preview');
const canvas = document.getElementById('render');
/*game_canvas.width = 1000;
game_canvas.height = 1000;*/
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
//const ctx_preview = game_canvas.getContext('2d');
const ctx = canvas.getContext('2d');
var scene;
var loop;
initialize();

// Variables pour les mobiles

var mobile_color;

// Variables pour les PC

function redrawcanvas() {
    try {
        window[scene + '_loop']();
    } catch (e) {
        console.error(e);
        console.log(scene);
    }
}

function changeScene(newScene) {
    clearInterval(loop);
    scene = newScene;
    try {
        window[scene]();
    } catch (e) {
        console.error(e);
    }
    redrawcanvas();
    //Met un display none à toutes les scenes dans le DOM
    var scene_children = document.getElementById("scenes").children;
    for (var i = 0; i < scene_children.length; i++) {
        scene_children[i].style.display = "none";
    }
    //Met un display block à la nouvelle scene dans le DOM
    document.getElementById(newScene).style.display = "block";
}

//Redimentionnement de la page --> redimentionnement du canvas
window.addEventListener('resize', function resizeCanvas() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    redrawcanvas();
}, false);

function initialize() {
    if (!('ontouchstart' in document.documentElement)) {
        changeScene('hello');
    } else {
        changeScene('hello_mobile');
    }
}

function background(r, g, b) {
    ctx.fillStyle = 'rgb(' + r + ', ' + g + ', ' + b + ')';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}
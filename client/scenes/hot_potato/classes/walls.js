class Wall {
    constructor(x, y, dx, dy) {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
    }

    draw() {
        ctx.fillStyle = 'rgb(0, 0, 0)';
        ctx.fillRect(this.x - viewX, this.y - viewY, this.dx, this.dy);
    }
}
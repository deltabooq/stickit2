class Player {
    constructor(color) {
        this.x = 0;
        this.y = 0;
        this.dx = 50;
        this.dy = 90;
        this.inertie_x = 0;
        this.inertie_y = 0;
        this.color = color;
        this.gravity = 0;
        this.max_gravity = 8;
        this.gravity_strength = 1;
        this.direction = 0;
        this.jump_amount = 18;
    }

    draw() {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x - viewX, this.y - viewY, this.dx, this.dy);
    }

    apply_inerties() {
        this.x += this.inertie_x;
        this.y += this.inertie_y;
        this.y += this.gravity;
    }

    attenuate_inerties() {
        //var inerties_attenuation = 0.3;
        var inerties_attenuation = 0.3;
        if (this.inertie_y < -inerties_attenuation / 2) this.inertie_y += inerties_attenuation;
        if (this.inertie_y > inerties_attenuation / 2) this.inertie_y -= inerties_attenuation;
        if (this.inertie_y >= -inerties_attenuation / 2 && this.inertie_y <= inerties_attenuation / 2) this.inertie_y = 0;
        if (this.inertie_x < -inerties_attenuation / 2) this.inertie_x += inerties_attenuation;
        if (this.inertie_x > inerties_attenuation / 2) this.inertie_x -= inerties_attenuation;
        if (this.inertie_x >= -inerties_attenuation / 2 && this.inertie_x <= inerties_attenuation / 2) this.inertie_x = 0;
        console.log(this.inertie_x + ' ' + this.inertie_y + ' ' + inerties_attenuation * 15);

    }

    calculate_gravity() {
        if (this.gravity < this.max_gravity) {
            this.gravity += this.gravity_strength;
        }
    }

    jump() {
        this.inertie_y = -this.jump_amount;
    }

    collision_walls(walls) {
        for (var i = 0; i < walls.length; i++) {
            var wall = walls[i];
            if (this.x + this.dx > wall.x + 3 && this.x < wall.x + wall.dx - 3) {
                if (this.y + this.dy > wall.y && this.y < wall.y + 3) {
                    //Touche le haut du wall -----------------------------------------------------------------------------------
                    //On téléporte le joueur en haut de la plateforme (+0.01 pour qu'il détecte toujours qu'il est sur la plateforme)
                    this.y = wall.y - this.dy + 0.01;
                    //On remet sa gravité à 0 (--> APPLICATION DE LA FORCE NORMALE)
                    this.gravity = 0;
                    return;
                } else if (this.y + this.dy > wall.y + wall.dy - 3 && this.y < wall.y + wall.dy) {
                    //Touche le bas du wall -----------------------------------------------------------------------------------
                    //On annule au joueur la force qui le propulse vers le haut
                    this.inertie_y = 0;
                    return;
                }
            }
            if (this.y + this.dy > wall.y && this.y < wall.y + wall.dy) {
                if (this.x + this.dx > wall.x && this.x < wall.x + 3) {
                    //Touche le côté gauche du wall -----------------------------------------------------------------------------------
                    this.x = wall.x - this.dx;
                    this.inertie_x = 0;
                } else if (this.x + this.dx > wall.x + wall.dx - 3 && this.x < wall.x + wall.dx) {
                    //touche le côté droit du wall -----------------------------------------------------------------------------------
                    this.x = wall.x + wall.dx;
                    this.inertie_x = 0;
                }
            }
        }
    }
}
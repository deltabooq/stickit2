var players = {};
var walls = [];
var viewX = 0;
var viewY = 0;

function hot_potato() {
    socket.emit('hot_potato_start_require_info');
}

function hot_potato_loop() {
    //Calcul de la nouvelle frame
    for (var player_socket_id in players) {
        var player = players[player_socket_id];
        player.calculate_gravity();
        //Réduit constamment les inerties X et Y du joueur vers 0
        player.attenuate_inerties();
        //Met à jour la position du joueur selon la gravité & les déplacements qu'il fait
        player.apply_inerties();
        //Calcul de la position en collision avec les murs
        player.collision_walls(walls);
    }
    players_calculate_mouvements();
    //collision_players_walls();
    //players_apply_movements();
    //Affichage de la frame
    background(57, 61, 76);
    draw_players();
    draw_walls();
}


function players_apply_inertie_ements() {
    for (var player_socket_id in players) {

    }
}

function players_calculate_mouvements() {
    //Donne une vitesse progressive aux joueurs lorsqu'ils se déplacent à gauche ou droite
    for (var player_socket_id in players) {
        if (players[player_socket_id].inertie_x < players[player_socket_id].direction) {
            players[player_socket_id].inertie_x += 0.5;
        } else if (players[player_socket_id].inertie_x > players[player_socket_id].direction) {
            players[player_socket_id].inertie_x -= 0.5;
        }
    }
}

function draw_players() {
    for (var player_socket_id in players) {
        players[player_socket_id].draw();
    }
}

function draw_walls() {
    for (var i = 0; i < walls.length; i++) {
        walls[i].draw();
    }
}

socket.on('hot_potato_start_give_info', (e) => {
    //Démarrage du jeu --> quand la partie a reçu les infos du serveur
    for (var player_socket_id in e.players) {
        var player_color = e.players[player_socket_id].color;
        var player = new Player(player_color);
        players[player_socket_id] = player;
    }
    walls.push(new Wall(0, 300, 300, 150));
    walls.push(new Wall(0, 650, canvas.width, 50));
    walls.push(new Wall(canvas.width - 50, 0, 50, canvas.height));
    loop = setInterval(hot_potato_loop, 10);
    load_arena_1();
    start_recieve_mobile_commands();
});

function start_recieve_mobile_commands() {
    socket.on('player_move', (e) => {
        switch (e.move) {
            case 'jump':
                players[e.socket_id].jump();
                break;
            default:
                players[e.socket_id].direction = e.move;
                console.log(players[e.socket_id].direction + ' ' + e.move);
                break;
        }
    });
}

function hot_potato_player_unload(e) {
    console.log('player ' + e.socket_id + ' has disconnected');
    delete players[e.socket_id];
}
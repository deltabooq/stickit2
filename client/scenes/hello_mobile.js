var div_token = document.getElementById('hello_mobile_token_game');

function hello_mobile() {
    window.addEventListener('resize', function resizeCanvas() {
        check_orientation();
    });
    check_orientation();
}

function hello_mobile_loop() {
    background(57, 61, 76);
}

function check_orientation() {
    if (window.innerWidth < window.innerHeight) {
        document.getElementById('mobile_popup_orientation').style.display = 'flex';
    } else {
        document.getElementById('mobile_popup_orientation').style.display = 'none';
    }
}

function hello_mobile_enter_token(number) {
    if (div_token.innerHTML.length == 3) {
        div_token.innerHTML += ' ';
    }
    if (div_token.innerHTML.length == 6) {
        hello_mobile_check_game_exists(div_token.innerHTML + number);
    }
    if (div_token.innerHTML.length != 7) {
        div_token.style.borderBottomColor = 'white';
    }

    div_token.innerHTML = (div_token.innerHTML + number).substr(0, 7);
}

function hello_mobile_erase_token() {
    if (div_token.innerHTML.length == 4) {
        div_token.innerHTML = div_token.innerHTML.slice(0, -1);
    }
    div_token.style.borderBottomColor = 'white';
    div_token.innerHTML = div_token.innerHTML.slice(0, -1);
}

function hello_mobile_check_game_exists(token) {
    socket.emit('hello_mobile_check_game_exists', token.substr(0, 3) + token.substr(4, 7));
}

socket.on('hello_mobile_check_game_exists_answer', (e) => {
    if (e.answer) {
        div_token.style.borderBottomColor = '#34c383';
        mobile_color = e.color;
        changeScene('controls');
    } else {
        div_token.style.borderBottomColor = '#ce3a3a';
    }
});
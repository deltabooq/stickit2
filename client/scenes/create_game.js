function create_game() {
    //Retour
    document.getElementById('create_game_retour').onclick = function() {
            changeScene('hello');
        }
        //Submit
    document.getElementById('create_game_form').addEventListener("submit", (e) => {
        e.preventDefault();
        var input = document.getElementById('create_game_name');
        if (input.value.length > 20) {
            alert('Le nom ne doit pas dépasser 20 caractères')
        } else if (input.value.length < 3) {
            alert('Le nom doit faire minimum 3 charactères')
        } else {
            socket.emit('create_game_submit', input.value);
        }
    });
}

function create_game_loop() {
    background(57, 61, 76);
    ctx.fillStyle = 'rgb(35, 38, 49)';
    ctx.fillRect(0, canvas.height - 100, canvas.clientWidth, 100);
}

socket.on('create_game_done', (e) => {
    changeScene('waiting_room');
    document.getElementById('waiting_room_name').innerHTML = '"' + e.name + '"';
    document.getElementById('waiting_room_token').innerHTML = (e.token + '').substr(0, 3) + ' ' + (e.token + '').substr(3, 3);
})
var acc_y = 0;
var sensitivity = 3;
var mobile_os = "";

function controls() {
    controls_detect_controls();
    controls_detect_acceleration();
    controls_detect_unload();
    controls_detect_pc_unload();
    mobile_os = detect_mobile_os();
}

function controls_loop() {
    ctx.fillStyle = mobile_color;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function controls_detect_controls() {
    document.getElementById('controls_jump').onclick = function() {
        socket.emit('controls_move', { move: 'jump' });
    }
    document.getElementById('controls_left').onclick = function() {
        socket.emit('controls_move', { move: -2 });
    }
    document.getElementById('controls_still').onclick = function() {
        socket.emit('controls_move', { move: 0 });
    }
    document.getElementById('controls_right').onclick = function() {
        socket.emit('controls_move', { move: 2 });
    }
}

function controls_detect_acceleration() {
    window.ondevicemotion = function(e) {
        var old_acc_y = acc_y;
        acc_y = parseInt(e.accelerationIncludingGravity.y);
        if (mobile_os == "iOS") acc_y = -acc_y;
        if (acc_y != old_acc_y) {
            document.getElementById('controls_test').innerHTML = acc_y;
            if (acc_y > 4) acc_y = 4;
            socket.emit('controls_move', { move: acc_y });
        }
    }
}

function controls_detect_unload() {
    window.onbeforeunload = function() {
        socket.emit('controls_unload');
    };
}

function controls_detect_pc_unload() {
    socket.on('pc_unload', (e) => {
        alert('Le serveur s\'est déconnecté');
        location.reload();
    });
}

this.mousePressed = function() {
    var fs = fullscreen();
    if (!fs) {
        fullscreen(true);
    }
}

function detect_mobile_os() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    // Windows Phone must come first because its UA also contains "Android"
    //if (/windows phone/i.test(userAgent)) {
    //  return "Windows Phone";
    //}
    if (/android/i.test(userAgent)) {
        return "Android";
    }
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "iOS";
    }
    return "unknown";
}
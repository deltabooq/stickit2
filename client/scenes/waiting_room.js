function waiting_room() {
    waiting_room_start_recieve_unload();
    waiting_room_detect_unload();
    document.getElementById('waiting_room_start_game').onclick = function() {
        changeScene('hot_potato');
    }
}

function waiting_room_loop() {
    background(57, 61, 76);
}

socket.on('waiting_room_player_joined', (e) => {
    console.log(e);
    update_nbr_players_connected(e.nb_players_connected);
});

function update_nbr_players_connected(nbr_connected) {
    document.getElementById('waiting_room_count_players_connected').innerHTML = nbr_connected + '/8 connectés';
    if (nbr_connected >= 1 /* A MODIFIER */) {
        document.getElementById('waiting_room_start_game').style.display = 'block';
    } else {
        document.getElementById('waiting_room_start_game').style.display = 'none';
    }
}

function waiting_room_player_unload(e) {
    console.log('player ' + e.socket_id + ' has disconnected');
    update_nbr_players_connected(e.nb_players_connected);
}

function waiting_room_start_recieve_unload() {
    socket.on('player_unload', (e) => {
        console.log('player unloaded');
        window[scene + '_player_unload'](e);
    });
}

function waiting_room_detect_unload() {
    window.onbeforeunload = function () {
        socket.emit('pc_unload');
    };
}